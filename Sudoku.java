

public class Sudoku {
    private int[] plan;

    public Sudoku(String initialState) {

        plan = new int[81];
        for (int i = 0; i < initialState.length(); i++) {
            char c = initialState.charAt(i);
            plan[i] = Character.getNumericValue(c);
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < 81; i++) {
            result.append(plan[i] + " ");
            if ((i + 1) % 9 == 0) {
                result.append("\n");
            }
        }
        return result.toString();
    }

    public static void main(String[] args) {
        String initialState = "530070000600195000098000060800060003400803001700020006060000280000419005000080079";
        Sudoku sudoku = new Sudoku(initialState);
        System.out.println(sudoku);
        sudoku.loese(0);
        System.out.println(sudoku);
    }
    
    public boolean loese(int kaestNr) {
    if (kaestNr == 81) {
        return true; // Lösung gefunden
    }

    if (plan[kaestNr] == 0) {
        // Kästchen ist leer, versuche alle möglichen Werte
        for (int wert = 1; wert <= 9; wert++) {
            plan[kaestNr] = wert; // Setze den Wert in das Kästchen ein
            if (istKonfliktfrei()) { // Überprüfe, ob der Wert gültig ist
                if (loese(kaestNr + 1)) {
                    return true; // Rekursiver Aufruf für das nächste Kästchen
                }
            }
        }
        // Keine gültige Lösung gefunden, setze das Kästchen wieder auf 0 zurück
        plan[kaestNr] = 0;
        return false;
    } else {
        // Kästchen ist bereits belegt, gehe zum nächsten Kästchen
        return loese(kaestNr + 1);
    }
}

    
    public boolean istKonfliktfrei() {
    // Überprüfe Zeilen
    for (int row = 0; row < 9; row++) {
        boolean[] vorhanden = new boolean[10];
        for (int col = 0; col < 9; col++) {
            int zahl = plan[row * 9 + col];
            if (zahl != 0) {
                if (vorhanden[zahl]) {
                    return false; // Konflikt gefunden
                }
                vorhanden[zahl] = true;
            }
        }
    }

    // Überprüfe Spalten
    for (int col = 0; col < 9; col++) {
        boolean[] vorhanden = new boolean[10];
        for (int row = 0; row < 9; row++) {
            int zahl = plan[row * 9 + col];
            if (zahl != 0) {
                if (vorhanden[zahl]) {
                    return false; // Konflikt gefunden
                }
                vorhanden[zahl] = true;
            }
        }
    }

    // Überprüfe 3x3-Blöcke
    for (int blockRow = 0; blockRow < 3; blockRow++) {
        for (int blockCol = 0; blockCol < 3; blockCol++) {
            boolean[] vorhanden = new boolean[10];
            for (int row = 0; row < 3; row++) {
                for (int col = 0; col < 3; col++) {
                    int zahl = plan[(blockRow * 3 + row) * 9 + (blockCol * 3 + col)];
                    if (zahl != 0) {
                        if (vorhanden[zahl]) {
                            return false; // Konflikt gefunden
                        }
                        vorhanden[zahl] = true;
                    }
                }
            }
        }
    }

    return true; // Keine Konflikte gefunden
}

}
