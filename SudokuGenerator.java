import java.util.Random;

public class SudokuGenerator {
    private static final int SIZE = 9;

    public static String addRandomNumbers(String sudokuString) {
        StringBuilder stringBuilder = new StringBuilder(sudokuString);

        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            int index = random.nextInt(SIZE * SIZE); // Random index between 0 and 80
            char currentChar = stringBuilder.charAt(index);

            if (currentChar == '0') {
                int randomNumber = random.nextInt(9) + 1; // Random number between 1 and 9
                if (isValidMove(stringBuilder.toString(), index / SIZE, index % SIZE, Character.forDigit(randomNumber, 10))) {
                    stringBuilder.setCharAt(index, Character.forDigit(randomNumber, 10));
                }
            }
        }

        return stringBuilder.toString();
    }

    private static boolean isValidMove(String sudokuString, int row, int col, char num) {
        // Check row
        for (int i = 0; i < SIZE; i++) {
            if (sudokuString.charAt(row * SIZE + i) == num) {
                return false;
            }
        }

        // Check column
        for (int i = 0; i < SIZE; i++) {
            if (sudokuString.charAt(i * SIZE + col) == num) {
                return false;
            }
        }

        // Check 3x3 block
        int startRow = row - row % 3;
        int startCol = col - col % 3;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (sudokuString.charAt((startRow + i) * SIZE + (startCol + j)) == num) {
                    return false;
                }
            }
        }

        return true;
    }

    public static void main(String[] args) {
        String clearString = "000000000000000000000000000000000000000000000000000000000000000000000000000000000";
        String sudokuString = addRandomNumbers(clearString);
        System.out.println(sudokuString);
        Sudoku sudoku = new Sudoku(sudokuString);
        System.out.println(sudoku.toString());
        sudoku.loese(0);
        System.out.println(sudoku.toString());
    }

}
